﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DBAccess;
using Models;

namespace WebUI.Common
{
    public class CategoryTree
    {
        public static MvcHtmlString GetTreeMenu(Category category)
        {
            //生成Html脚本
            var htmlString = new StringBuilder();
            if (category == null)
                return null;

            htmlString.Append("<ul>");
            //把当前分类的子类查询出来
            var sonList = new DataContext().Categories.Where(n => n.ParentCategory.ID == category.ID)
                .OrderBy(n => n.SortCode).ToList();

            foreach (var item in sonList)
            {
                htmlString.Append("<li><a href='../../categories/index/"+item.ID+"'>");
                htmlString.Append(item.Name);
                htmlString.Append("</a></li>");

                //递归继续遍历子类的子类，直到没有子类为止
                htmlString.Append(GetTreeMenu(item));
            }
            htmlString.Append("</ul>");
            
            MvcHtmlString htmlContent= new MvcHtmlString(htmlString.ToString());
            return htmlContent;
        }
    }
}