﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //开启特性路由
            routes.MapMvcAttributeRoutes();

            ////路由的表的注册按上到下的次序匹配，特殊的规则放最上
            //routes.MapRoute("blog",
            //    "blog/{year}/{month}/{day}",
            //    new {controller = "Test", action = "Index", year = "2020", month = "1", day = "1"},
            //    //限制year只能是2位或4位数字
            //    new
            //    {
            //        year = @"\d{2}|\d{4}",
            //        month = @"\d{1,2}",
            //        day = @"\d{1,2}"
            //    });


            ////访问my test/index
            //routes.MapRoute("MyRout1", "my/{id}", new {Controller = "test", Action = "index",id=UrlParameter.Optional});
            ////可变长的路由
            //routes.MapRoute("MyRout2", "{controller}/{action}/{id}/{*.catchall}",
            //    new {controller = "test", action = "Index", id = UrlParameter.Optional});


            //定义路由规则
            routes.MapRoute(
                name: "Default",   //规则名
                url: "{controller}/{action}/{id}",   //URL分段定义
                //缺省的路由访问地址  www.lzzy.net  ->   www.lzzy.net/home/index  UrlParameter.Optional可选参数
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
