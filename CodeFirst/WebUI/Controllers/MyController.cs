﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBAccess;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Models.UserAndRole;
using ViewModels;

namespace WebUI.Controllers
{
    public class MyController : Controller
    {
        private static readonly DataContext _context = new DataContext();

        [Authorize]
        // 我的资料管理
        public ActionResult Index()
        {
            if (Session["LoginUser"] == null)
                return RedirectToAction("login", "Account", new {ReturnUrl = Url.Action("index", "My")});

            var person = (Session["LoginUser"] as ApplicationUser).Person;

            //把person转换成myVM
            var model = new MyVM()
            {
                 FirstName = person.FirstName,
                 LastName = person.LastName,
                 Address = person.Address,
                 Birthday = person.Birthday,
                 CredentialsCode = person.CredentialsCode,
                 Email = person.Email,
                 MobileNumber = person.MobileNumber,
                 Sex = person.Sex
            };
            ViewBag.AvardaURL = person.Avarda;

            return View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(MyVM model)
        {
            //判断用户登录会话是否过期
            if (Session["LoginUser"] == null)
                return RedirectToAction("login", "Account", new { ReturnUrl = Url.Action("index", "My") });

            //查询原个人信息
            var person = _context.Persons.Find((Session["LoginUser"] as ApplicationUser).Person.ID);
            //把原头像暂存
            var oldAvarda = person.Avarda;

            if (ModelState.IsValid)
            {
                if (model.Avarda != null)
                {
                    //数据验证是正确的，进行数据修改
                    //1.头像上传
                    var dir = "~/upload/avarda";
                    //把上传的文件重命名，重命名为person.ID，保证文件名是唯一
                    //取原始文件的后缀名
                    var fileLastName = model.Avarda.FileName.Substring(model.Avarda.FileName.LastIndexOf(".") + 1,
                        model.Avarda.FileName.Length - model.Avarda.FileName.LastIndexOf(".") - 1);
                    //创建成新文件的物理全路径
                    var imgPath = Path.Combine(Server.MapPath(dir), person.ID + "." + fileLastName);

                    //上传文件，保存到服务器的物理路径
                    model.Avarda.SaveAs(imgPath);
                    oldAvarda = "/upload/avarda/" + person.ID + "." + fileLastName;
                }

                //2.个人信息更新到数据库
                person.FirstName = model.FirstName;
                person.LastName = model.LastName;
                person.Name = model.FirstName + model.LastName;
                person.Email = model.Email;
                person.MobileNumber = model.MobileNumber;
                person.Sex = model.Sex;
                person.Birthday = model.Birthday;
                person.Address = model.Address;
                person.Avarda = oldAvarda;
                person.CredentialsCode = model.CredentialsCode;
                person.UpdateTime = DateTime.Now;

                _context.SaveChanges();

                //刷新当前页
                var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new DataContext()));
                var user = um.FindById((Session["LoginUser"] as ApplicationUser).Id);
                Session["LoginUser"] = user;
                return RedirectToAction("Index");

            }

            ViewBag.AvardaURL = oldAvarda;
            return View(model);
        }

    }
}