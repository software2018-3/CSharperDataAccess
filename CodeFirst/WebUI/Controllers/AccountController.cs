﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebSockets;
using DBAccess;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Models.UserAndRole;
using ViewModels;

namespace WebUI.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="ReturnUrl">登录成功后跳转回原访问地址</param>
        /// <returns></returns>
        public ActionResult Login(string ReturnUrl=null)
        {
            if (string.IsNullOrEmpty(ReturnUrl))
                ViewBag.ReturnUrl = Url.Action("index", "Home");
            else
                ViewBag.ReturnUrl = ReturnUrl;

            return View();
        }

        //此Action用来接收login视图的提交数据
        [HttpPost]
        //制定签名
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM model,string ReturnUrl)
        {
            //用户输入的数据是正确
            if (ModelState.IsValid)
            {
                var loginStatus = new LoginUserStatus()
                {
                    IsLogin = false,
                    LoginMessage = "用户名或密码错误",
                    ErrorCode = "009"
                };

                //登录处理
                var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new DataContext()));
                //密码是加密的，只能用UserManager中的方法
                var user = um.Find(model.UserName, model.PassWord);
                if (user != null)
                {
                    //如果登录成功，则获得角色
                    var roleName = "";
                    var context = new DataContext();
                    foreach (var role in user.Roles)
                    {
                        roleName += ((context.Roles.Find(role.RoleId)) as ApplicationRole).DisplayName + ",";
                    }
                    //去掉最后一个,
                    roleName = roleName.Substring(0, roleName.Length - 1);


                    loginStatus.IsLogin = true;
                    loginStatus.LoginMessage = "登录成功，用户的角色：" + roleName;
                    loginStatus.ErrorCode = "";

                    Session["LoginUser"] = user;
                    Session["LoginStatus"] = loginStatus;

                    //放发identity令牌
                    var identityToken = um.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                    //对此用户授权可访问过滤的控制器或Action
                    //FormsAuthentication.SetAuthCookie(user.UserName, false);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                        1,
                        user.UserName,
                        DateTime.Now, 
                        DateTime.Now.AddMinutes(20),
                        true,
                        roleName,
                        "/"
                    );
                    //把ticket保存到cookie,  加密
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName,FormsAuthentication.Encrypt(ticket));
                    //每个用户的票据必须唯一
                    cookie.HttpOnly = true;
                    HttpContext.Response.Cookies.Add(cookie);

                    return Redirect(ReturnUrl);
                }
                else
                {
                    if (string.IsNullOrEmpty(ReturnUrl))
                        ViewBag.ReturnUrl = Url.Action("index", "Home");
                    else
                        ViewBag.ReturnUrl = ReturnUrl;

                    return View();
                }
            }
            if (string.IsNullOrEmpty(ReturnUrl))
                ViewBag.ReturnUrl = Url.Action("index", "Home");
            else
                ViewBag.ReturnUrl = ReturnUrl;

            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(SignUpVM model)
        {
            if (ModelState.IsValid)
            {
                //注册操作
                var person = new Person()
                {
                    FirstName = model.FisrtName,
                    LastName = model.LastName,
                    Name = model.FisrtName + model.LastName,
                    CredentialsCode = "",
                    Birthday = DateTime.Now,
                    Sex = true,
                    MobileNumber = "",
                    Email = model.Email,
                    TelephoneNumber = "",
                    Description = "",
                    CreateDateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                    InquiryPassword = "未设置"
                };
                var user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    FirstName = model.FisrtName,
                    LastName = model.LastName,
                    ChineseFullName = model.FisrtName + model.LastName,
                    MobileNumber = "",
                    Email = model.Email,
                    Person = person
                };
                //验证邮箱是否唯一
                var mailInPerson = new DataContext().Persons.SingleOrDefault(n => n.Email == model.Email.Trim());
                if (mailInPerson != null)
                {
                    return Content("<script>alert('邮箱必须唯一，请重试！');location.href='" + Url.Action("SignUp") +
                                   "'</script>");
                }

                //保存
                var idm = new IdentityManager();
                idm.CreateUser(user, model.PassWord);
                idm.AddUserToRole(user.Id, "CommonUser");

                return Content("<script>alert('注册成功请登录！');location.href='" + Url.Action("Login") + "'</script>");
            }

            return View();
        }

        public ActionResult SignOut()
        {
            //会话清空
            Session["LoginUser"] = null;
            Session["LoginStatus"] = null;

            //cookie清空
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        public ActionResult ChangePassWord()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassWord(ChangePwdVM model)
        {
            if (ModelState.IsValid)
            {
                //修改密码操作
                bool changePwdStatus = false;
                try
                {
                    //验证原密码
                    //当前登录用户名
                    if(Session["LoginUser"]==null)
                        //如果当前不存在登录会话，重新登录
                        return RedirectToAction("Login");

                    var currentUserName = (Session["LoginUser"] as ApplicationUser).UserName;
                    var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new DataContext()));
                    var user = um.Find(currentUserName, model.PassWord);

                    if (user == null)
                    {
                        //原密码错误，提示用户
                        ModelState.AddModelError("","原密码错误，请重试");
                        return View(model);
                    }
                    else
                    {
                        //修改为新密码
                        changePwdStatus = um.ChangePassword(user.Id, model.PassWord, model.NewPassWord).Succeeded;
                        if (changePwdStatus)
                            return RedirectToAction("Login");
                        else
                            ModelState.AddModelError("", "密码修改错误，请重试");
                    }
                }
                catch
                {
                    //原密码错误，提示用户
                    ModelState.AddModelError("", "密码修改错误，请重试");
                }

                return RedirectToAction("Login");
            }

            return View(model);
        }
    }
}