﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        [Route("tt.html")]
        public ActionResult Index()
        {
            ViewBag.Controller = RouteData.Values["controller"];
            ViewBag.Action = RouteData.Values["action"];
            ViewBag.ID = RouteData.Values["id"];
            ViewBag.Year = RouteData.Values["year"];
            ViewBag.Month = RouteData.Values["month"];
            ViewBag.Day = RouteData.Values["day"];
            return View();
        }
    }
}