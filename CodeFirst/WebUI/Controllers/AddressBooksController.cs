﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBAccess;
using Models;
using Models.UserAndRole;

namespace WebUI.Controllers
{
    public class AddressBooksController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AddressBooks
        [Authorize]
        public ActionResult Index(int pageIndex=1)
        {
            //是否有登录会话
            if (Session["LoginUser"] == null)
                return RedirectToAction("login", "Account", new { ReturnUrl = Url.Action("index", "AddressBooks") });

            var person = (Session["LoginUser"] as ApplicationUser).Person;

            //每页显示的记录数
            var pageSize = 5;


            var list = db.AddressBooks.Where(n=>n.Person.ID==person.ID).OrderBy(n=>n.Name).ToList();
            //计算总数
            var recordCount = list.Count;
            //如果记录为空
            if (recordCount == 0)
            {
                //当前页
                ViewBag.PageIndex = 1;
                //每页的记录数
                ViewBag.PageSize = pageSize;
                //所有记录的总数
                ViewBag.TotalCount = recordCount;
                return View(list);
            }

            //记录不为0
            //总的页数
            int pageCount = Convert.ToInt32(Math.Ceiling((double)recordCount / pageSize));
            //当前页 不能小于1 不能大于PageCount
            pageIndex = pageIndex < 1 ? 1 : pageIndex;
            pageIndex = pageIndex > pageCount ? pageCount : pageIndex;

            //当前页
            ViewBag.PageIndex = pageIndex;
            //每页的记录数
            ViewBag.PageSize = pageSize;
            //所有记录的总数
            ViewBag.TotalCount = recordCount;

            //提取出当前页的记录
            var pageList = db.AddressBooks.Where(n=>n.Person.ID==person.ID).OrderBy(n=>n.Name)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .ToList();

            return View(pageList);
        }

        [HttpPost]
        public ActionResult Index()
        {
            //接收表单中的参数，提取关键词
            var kw = Request.Form["keys"];
            if(string.IsNullOrEmpty(kw))
                return RedirectToAction("Index", "AddressBooks", new {pageIndex = 1});

            //当前会话是否是登录状态
            if (Session["LoginUser"] == null)
                return RedirectToAction("login", "Account", new { ReturnUrl = Url.Action("index", "AddressBooks") });

            var person = (Session["LoginUser"] as ApplicationUser).Person;


            var list = db.AddressBooks
                .Where(n => n.Person.ID == person.ID && (
                    n.Name.Contains(kw)) || n.Mobil.Contains(kw) || n.Phone.Contains(kw)
                )
                .OrderBy(n => n.Name).ToList();

            //当前页
            ViewBag.PageIndex = 1;
            //每页的记录数
            ViewBag.PageSize = 10000;
            //所有记录的总数
            ViewBag.TotalCount = list.Count;
            return View(list);
        }

        [Authorize]
        // GET: AddressBooks/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }

            return View(addressBook);
        }

        [Authorize]
        // GET: AddressBooks/Create
        public ActionResult Create(int pageIndex=1)
        {
            ViewBag.PageIndex = pageIndex;
            return View();
        }

        [Authorize]
        // POST: AddressBooks/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Phone,Mobil")]
            AddressBook addressBook)
        {
            //校验表单中的当前页是否为空，主要排除用户无理操作
            var pageIndex = Request.Form["pageIndex"]??"1";
            

            if (ModelState.IsValid)
            {
                addressBook.ID = Guid.NewGuid();
                //当前登录用户的person绑定到addressbook
                //是否有登录会话
                if (Session["LoginUser"] == null)
                    return RedirectToAction("login", "Account", new { ReturnUrl = Url.Action("index", "AddressBooks") });

                var person = (Session["LoginUser"] as ApplicationUser).Person;
                //更新数据时，外键必须从同一上下文中查询
                addressBook.Person = db.Persons.Find(person.ID);

                db.AddressBooks.Add(addressBook);
                db.SaveChanges();
                return RedirectToAction("Index", "AddressBooks", new {pageIndex = pageIndex});
            }

            return View(addressBook);
        }

        [Authorize]
        // GET: AddressBooks/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }

            return View(addressBook);
        }

        [Authorize]
        // POST: AddressBooks/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Phone,Mobil")]
            AddressBook addressBook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addressBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(addressBook);
        }

        [Authorize]
        // GET: AddressBooks/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }

            return View(addressBook);
        }

        [Authorize]
        // POST: AddressBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            AddressBook addressBook = db.AddressBooks.Find(id);
            db.AddressBooks.Remove(addressBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
