﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBAccess;
using Models;
using Models.UserAndRole;

namespace WebUI.Controllers
{
    [Authorize(Roles = "超级管理员组")]
    public class AddressBookManageController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AddressBookManage
        public ActionResult Index(string personID)
        {
            //是否有登录会话
            if (Session["LoginUser"] == null)
                return RedirectToAction("login", "Account", new {ReturnUrl = Url.Action("index", "AddressBookManage")});


            //判断会话中的角色是否是 “超级管理员”
            var user = Session["LoginUser"] as ApplicationUser;
            if (user != null)
            {
                //如果登录成功，则获得角色
                var roleName = "";
                var context = new DataContext();
                foreach (var role in user.Roles)
                {
                    roleName += ((context.Roles.Find(role.RoleId)) as ApplicationRole).DisplayName + ",";
                }

                //去掉最后一个,
                roleName = roleName.Substring(0, roleName.Length - 1);

                if(!roleName.Contains("超级管理员组"))
                     return RedirectToAction("login", "Account", new {ReturnUrl = Url.Action("index", "AddressBookManage")});
            }


            var persons = db.Persons.ToList();
            ViewBag.Persons = persons;

            //按照personID来进行数据筛选
            if(string.IsNullOrEmpty(personID))
                return View(db.AddressBooks.OrderBy(n=>n.Name).ToList());
            else
            {
                Guid id=Guid.Parse(personID);
                return View(db.AddressBooks.Where(n => n.Person.ID ==id).OrderBy(n=>n.Name).ToList());
            }
        }

        // GET: AddressBookManage/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }

        // GET: AddressBookManage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AddressBookManage/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Phone,Mobil")] AddressBook addressBook)
        {
            if (ModelState.IsValid)
            {
                addressBook.ID = Guid.NewGuid();
                db.AddressBooks.Add(addressBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(addressBook);
        }

        // GET: AddressBookManage/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }

        // POST: AddressBookManage/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Phone,Mobil")] AddressBook addressBook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addressBook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(addressBook);
        }

        // GET: AddressBookManage/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AddressBook addressBook = db.AddressBooks.Find(id);
            if (addressBook == null)
            {
                return HttpNotFound();
            }
            return View(addressBook);
        }

        // POST: AddressBookManage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            AddressBook addressBook = db.AddressBooks.Find(id);
            db.AddressBooks.Remove(addressBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
