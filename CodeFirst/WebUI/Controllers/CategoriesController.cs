﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DBAccess;
using Models;

namespace WebUI.Controllers
{
    public class CategoriesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Categories
        public ActionResult Index(Guid? id,int pageIndex=1)
        {
            //每页显示的记录数
            var pageSize = 5;

            //查询顶级分类
            var topCategories = db.Categories.Where(n => n.ParentCategory == null).ToList();
            //动态传值到视图
            ViewBag.Tops = topCategories;

            if (id == null)
            {
                //查询出所有的分类
                var list = db.Categories.ToList();
                //计算总数
                var recordCount = list.Count;
                //如果记录为空
                if (recordCount == 0)
                {
                    //当前页
                    ViewBag.PageIndex = 1;
                    //每页的记录数
                    ViewBag.PageSize =pageSize;
                    //所有记录的总数
                    ViewBag.TotalCount = recordCount;
                    return View(list);
                }

                //记录不为0
                //总的页数
                int pageCount = Convert.ToInt32(Math.Ceiling((double) recordCount / pageSize));
                //当前页 不能小于1 不能大于PageCount
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageIndex = pageIndex > pageCount ? pageCount : pageIndex;

                //当前页
                ViewBag.PageIndex = pageIndex;
                //每页的记录数
                ViewBag.PageSize = pageSize;
                //所有记录的总数
                ViewBag.TotalCount = recordCount;

                //提取出当前页的记录
                var pageList = db.Categories.OrderBy(n => n.SortCode).Skip((pageIndex - 1) * pageSize).Take(pageSize)
                    .ToList();
                return View(pageList);
            }
            else
            {
                var list = db.Categories.Where(n => n.ParentCategory.ID == id).OrderBy(n => n.SortCode).ToList();
                //计算总数
                var recordCount = list.Count;
                //如果记录为空
                if (recordCount == 0)
                {
                    //当前页
                    ViewBag.PageIndex = 1;
                    //每页的记录数
                    ViewBag.PageSize = pageSize;
                    //所有记录的总数
                    ViewBag.TotalCount = recordCount;
                    return View(list);
                }
                
                //记录不为0
                //总的页数
                int pageCount = Convert.ToInt32(Math.Ceiling((double)recordCount / pageSize));
                //当前页 不能小于1 不能大于PageCount
                pageIndex = pageIndex < 1 ? 1 : pageIndex;
                pageIndex = pageIndex > pageCount ? pageCount : pageIndex;

                //当前页
                ViewBag.PageIndex = pageIndex;
                //每页的记录数
                ViewBag.PageSize = pageSize;
                //所有记录的总数
                ViewBag.TotalCount = recordCount;

                var data = db.Categories.Where(n => n.ParentCategory.ID == id).OrderBy(n => n.SortCode)
                    .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                return View(data);
            }
        }

        // GET: Categories/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SortCode,Name,Dscn")] Category category)
        {
            if (ModelState.IsValid)
            {
                category.ID = Guid.NewGuid();
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // 为了防止“过多发布”攻击，请启用要绑定到的特定属性，有关 
        // 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SortCode,Name,Dscn")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
