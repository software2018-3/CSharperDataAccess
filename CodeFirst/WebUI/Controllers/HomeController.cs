﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
         [Authorize]    //要求此Action必须通过授权才允许访问
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Users = "messi")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //只允许让admin角色访问 怎么实现？
        [Authorize(Roles = "超级管理员组,一般管理员组")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Map()
        {
            return View();
        }
    }
}