﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebSockets;
using DBAccess;
using Models.UserAndRole;

namespace WebUI.Migrations
{
    /// <summary>
    /// 用户、角色数据初始化
    /// </summary>
    public class UserAndRoleSeed
    {
        private  static DataContext _context  = new DataContext();

        /// <summary>
        /// 添加角色
        /// </summary>
        public static void RoleSeed()
        {
            var role1= new ApplicationRole()
            {
                Name = "Admin",
                DisplayName = "超级管理员组",
                Description = "最高等级的管理账户",
                SortCode = "000",
                ApplicationRoleType = ApplicationRoleType.适用于系统管理人员
            };
            var role2 = new ApplicationRole()
            {
                Name = "Manager",
                DisplayName = "一般管理员组",
                Description = "某个业务板块的管理账户",
                SortCode = "001",
                ApplicationRoleType = ApplicationRoleType.适用于有管理权限用户
            };
            var role3 = new ApplicationRole()
            {
                Name = "CommonUser",
                DisplayName = "一般用户组",
                Description = "普通用户账户",
                SortCode = "002",
                ApplicationRoleType = ApplicationRoleType.适用于一般注册用户
            };
            var idm = new IdentityManager();
            idm.CreateRole(role1);
            idm.CreateRole(role2);
            idm.CreateRole(role3);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        public static void UserSeed()
        {
            var idm = new IdentityManager();

            #region 管理员
            var p1 = new Person()
            {
                FirstName = "梅",
                LastName = "西",
                Name = "梅西",
                CredentialsCode = "450101199001011234",
                Birthday = DateTime.Parse("1990-1-1"),
                Sex = true,
                MobileNumber = "13833883388",
                Email = "messi@163.com",
                CreateDateTime = DateTime.Now,
                TelephoneNumber = "3150110",
                Description = "我是梅西，我是管理员",
                UpdateTime = DateTime.Now,
                InquiryPassword = "123456"
            };
            var loginUser1 = new ApplicationUser()
            {
                UserName = "messi",  //登录名
                FirstName = "梅",
                LastName = "西",
                ChineseFullName = "梅西",
                MobileNumber = "13833883388",
                Email = "messi@163.com",
                Person = p1,   //把账号与个人信息进行关联
            };
            //分配密码
            idm.CreateUser(loginUser1, "123.abc");   
            //分配角色
            idm.AddUserToRole(loginUser1.Id, "Admin");

            #endregion

            #region 业务模块管理员
            var p2 = new Person()
            {
                FirstName = "许",
                LastName = "仙",
                Name = "许仙",
                CredentialsCode = "450101199901111334",
                Birthday = DateTime.Parse("1999-1-1"),
                Sex = true,
                MobileNumber = "13833443344",
                Email = "xuxian@163.com",
                CreateDateTime = DateTime.Now,
                TelephoneNumber = "3150110",
                Description = "我是许仙，我是论坛管理员",
                UpdateTime = DateTime.Now,
                InquiryPassword = "123456"
            };
            var loginUser2 = new ApplicationUser()
            {
                UserName = "xuxian",  //登录名
                FirstName = "许",
                LastName = "仙",
                ChineseFullName = "许仙",
                MobileNumber = "13833443344",
                Email = "xuxian@163.com",
                Person = p2,   //把账号与个人信息进行关联
            };
            //分配密码
            idm.CreateUser(loginUser2, "123.abc");
            //分配角色
            idm.AddUserToRole(loginUser2.Id, "Manager");

            var p3 = new Person()
            {
                FirstName = "虚",
                LastName = "竹",
                Name = "虚竹",
                CredentialsCode = "450101199901111114",
                Birthday = DateTime.Parse("1999-1-1"),
                Sex = true,
                MobileNumber = "13833446644",
                Email = "xuzhu@163.com",
                CreateDateTime = DateTime.Now,
                TelephoneNumber = "3150110",
                Description = "我是虚竹，我是电商平台管理员",
                UpdateTime = DateTime.Now,
                InquiryPassword = "123456"
            };
            var loginUser3 = new ApplicationUser()
            {
                UserName = "xuzhu",  //登录名
                FirstName = "虚",
                LastName = "竹",
                ChineseFullName = "虚竹",
                MobileNumber = "13833446644",
                Email = "xuzhu@163.com",
                Person = p3,   //把账号与个人信息进行关联
            };
            //分配密码
            idm.CreateUser(loginUser3, "123.abc");
            //分配角色
            idm.AddUserToRole(loginUser3.Id, "Manager");


            #endregion

            #region 普通用户
            var p5 = new Person()
            {
                FirstName = "黄",
                LastName = "先生",
                Name = "黄先生",
                CredentialsCode = "4501011999012221224",
                Birthday = DateTime.Parse("1999-1-1"),
                Sex = true,
                MobileNumber = "13822446644",
                Email = "huang@163.com",
                CreateDateTime = DateTime.Now,
                TelephoneNumber = "3150110",
                Description = "我是黄先生",
                UpdateTime = DateTime.Now,
                InquiryPassword = "123456"
            };
            var loginUser5 = new ApplicationUser()
            {
                UserName = "huang",  //登录名
                FirstName = "黄",
                LastName = "先生",
                ChineseFullName = "黄先生",
                MobileNumber = "13822446644",
                Email = "huang@163.com",
                Person = p5,   //把账号与个人信息进行关联
            };
            //分配密码
            idm.CreateUser(loginUser5, "123.abc");
            //分配角色
            idm.AddUserToRole(loginUser5.Id, "Manager");

            #endregion
        }
    }
}