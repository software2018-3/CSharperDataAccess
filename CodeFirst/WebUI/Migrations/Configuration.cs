﻿using Models;

namespace WebUI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DBAccess.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DBAccess.DataContext context)
        {
            ////context.Database.ExecuteSqlCommand("delete categories");
            ////CategorySeed.Seed(context);

            ////初始化角色 
            //UserAndRoleSeed.RoleSeed();

            ////初始化用户
            //UserAndRoleSeed.UserSeed();

            context.Database.ExecuteSqlCommand("delete AddressBooks");

            var person = context.Persons.SingleOrDefault(n => n.Name == "梅西");

            var a1= new AddressBook()
            {
                Name = "小白",
                Phone = "3150110",
                Mobil = "13507720110",
                Person = person
                
            };
            var a2= new AddressBook()
            {
                Name = "小红",
                Phone = "3150111",
                Mobil = "13507720111",
                Person = person
            };
            var a3 = new AddressBook()
            {
                Name = "小绿",
                Phone = "3150112",
                Mobil = "13507720112",
                Person = person
            };
            context.AddressBooks.Add(a1);
            context.AddressBooks.Add(a2);
            context.AddressBooks.Add(a3);
            context.SaveChanges();
        }
    }
}
