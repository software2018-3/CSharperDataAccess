﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DBAccess;
using Models;

namespace WebUI.Migrations
{
    public class CategorySeed
    {
        public static void Seed(DataContext context)
        {
            //顶级分类
            var c1 = new Category()
            {
                Name = "家用电器",
                SortCode = "01",
                Dscn = "电视、空调、洗衣机、冰箱……"
            };
            var c2 = new Category()
            {
                Name = "电脑",
                SortCode = "02",
                Dscn = "电脑整机、电脑配件、外设、笔记本电脑、平板……"
            };
            context.Categories.Add(c1);
            context.Categories.Add(c2);
            context.SaveChanges();

            //二级分类
            var c11 = new Category()
            {
                Name = "电视",
                Dscn = "全面屏、智能电视、4K电视……",
                SortCode = "0101",
                ParentCategory = context.Categories.SingleOrDefault(n=>n.Name=="家用电器")
            };
            var c12 = new Category()
            {
                Name = "空调",
                Dscn = "挂机、柜机、中央空调……",
                SortCode = "0102",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "家用电器")
            };
            var c21 = new Category()
            {
                Name = "笔记本",
                Dscn = "上网本、游戏本、移动工作站……",
                SortCode = "0201",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "电脑")
            };

            context.Categories.Add(c11);
            context.Categories.Add(c12);
            context.Categories.Add(c21);
            context.SaveChanges();

            var c111 = new Category()
            {
                Name = "全面屏",
                Dscn = "全面屏",
                SortCode = "010101",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "电视")
            };
            var c112 = new Category()
            {
                Name = "4K高清",
                Dscn = "4K高清",
                SortCode = "010102",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "电视")
            };

            var c121 = new Category()
            {
                Name = "挂式空调",
                Dscn = "挂式空调",
                SortCode = "010201",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "空调")
            };

            var c122 = new Category()
            {
                Name = "柜式空调",
                Dscn = "柜式空调",
                SortCode = "010202",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "空调")
            };

            var c211 = new Category()
            {
                Name = "上网本",
                Dscn = "上网本",
                SortCode = "020101",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "笔记本")
            };

            var c212 = new Category()
            {
                Name = "游戏本",
                Dscn = "游戏本",
                SortCode = "020102",
                ParentCategory = context.Categories.SingleOrDefault(n => n.Name == "笔记本")
            };
            context.Categories.Add(c111);
            context.Categories.Add(c112);
            context.Categories.Add(c121);
            context.Categories.Add(c122);
            context.Categories.Add(c211);
            context.Categories.Add(c212);
            context.SaveChanges();
        }
    }
}