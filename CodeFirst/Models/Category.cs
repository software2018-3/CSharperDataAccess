﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 商品分类的实体
    /// </summary>
    public class Category
    {
        public Guid ID { get; set; }
        public string SortCode { get; set; }
        public string Name { get; set; }
        public string Dscn { get; set; }
        //上级分类
        public virtual  Category ParentCategory { get; set; }

        public Category()
        {
            ID = Guid.NewGuid();
        }
    }
}
