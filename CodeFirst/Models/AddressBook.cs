﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.UserAndRole;

namespace Models
{
    public class AddressBook
    {
        public Guid ID { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "固定电话")]
        public string Phone { get; set; }
        
        [Display(Name = "移动话")]
        public string Mobil { get; set; }

        //归属人
        public virtual Person Person { get; set; }

        public AddressBook()
        {
            ID= Guid.NewGuid();
        }
    }
}
