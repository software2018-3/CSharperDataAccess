﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Models;
using Models.UserAndRole;

namespace DBAccess
{
    /// <summary>
    /// 数据映射
    /// 启用迁移的命令:
    /// enable-migrations -enableautomaticmigrations -startupprojectname WebUI -projectname WebUI -contextprojectname DBAccess -contexttypename DataContext
    /// </summary>
    //  IdentityDbContext<ApplicationUser>  用户和角色的上下文
    public class DataContext:IdentityDbContext<ApplicationUser>    
    {
        //关联数据连接串
        public DataContext():base("DataContext"){}

        //添加一个创建上下文的方法
        public static DataContext Create()
        {
            return  new DataContext();
        }

        public IDbSet<Category> Categories { get; set; }
        public IDbSet<AddressBook> AddressBooks { get; set; }

        #region  用户和角色的实体
        public IDbSet<ApplicationInformation> ApplicationInformations { get; set; }
        public IDbSet<ApplicationBusinessType> ApplicationBusinessTypes { get; set; }
        public IDbSet<ApplicaitionUserInApplication> ApplicaitionUserInApplications { get; set; }
        public IDbSet<Person> Persons { get; set; }

        #endregion
    }
}
