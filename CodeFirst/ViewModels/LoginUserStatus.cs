﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    /// <summary>
    /// 用户登录的状太实体
    /// </summary>
    public class LoginUserStatus
    {
        public bool IsLogin { get; set; }   //登录成功的状态
        public string LoginMessage { get; set; }   //登录状态的文本信息
        public string ErrorCode { get; set; }   //错误代码
    }
}
