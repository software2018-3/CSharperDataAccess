﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ViewModels
{
    public class MyVM
    {
        [StringLength(10,ErrorMessage = "{0}长度不能小于{1}大于{2}位",MinimumLength = 1)]
        [Required(ErrorMessage = "姓不能为空")]
        [Display(Name = "姓")]
        public string FirstName { get; set; } // 姓氏

        [StringLength(20, ErrorMessage = "{0}长度不能小于{1}大于{2}位", MinimumLength = 1)]
        [Required(ErrorMessage = "名不能为空")]
        [Display(Name = "名")]
        public string LastName { get; set; } // 名字
        
        [Display(Name = "性别")]
         [Required(ErrorMessage = "性别不能为空")]
        public bool Sex { get; set; } // 性别
        
        [Display(Name = "手机")]
        [Required(ErrorMessage = "名不能为空")]
        [RegularExpression(@"^1(3|4|5|6|7|8|9)\d{9}$",ErrorMessage = "请输入正确的手机格式")]
        public string MobileNumber { get; set; } // 手机号码
        
        [Required(ErrorMessage = "电子邮件不能为空")]
        [Display(Name = "电子邮箱")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$", ErrorMessage = "请输入正确的Email格式")]
        public string Email { get; set; } // 电子邮箱

        [Display(Name = "生日")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; } // 出生日期

        [Display(Name = "证件号")]
        [RegularExpression(@"(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)",ErrorMessage = "请输入正确的证件号")]
        public string CredentialsCode { get; set; } // 身份证编号

        [Display(Name = "地址")]
        public string Address { get; set; }

        [Display(Name = "头像")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Avarda { get; set; }   //用于处理把文件提交到服务器
    }
}
