﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    /// <summary>
    /// 登录的视图模型
    /// </summary>
    public class LoginVM
    {
        [Required(ErrorMessage = "用户名不能为空")]   //验证此字段不能为空
        [Display(Name = "用户名")]          //定义此字段的呈现名称
        public string UserName { get; set; }

        [Required(ErrorMessage = "密码不能为空")]   //验证此字段不能为空
        [Display(Name = "密码")]          //定义此字段的呈现名称
        public string PassWord { get; set; }
    }
}
