﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    /// <summary>
    /// 修改密码的视图模型
    /// </summary>
    public class ChangePwdVM
    {
        [Required(ErrorMessage = "原密码不能为空")]
        [DataType(DataType.Password)]
        [Display(Name = "原密码")]
        public string PassWord { get; set; }

        [Required(ErrorMessage = "新密码不能为空")]
        [DataType(DataType.Password)]
        [Display(Name = "新密码")]
        [StringLength(12,ErrorMessage = "{0}长度不能小于{1}大于{2}位",MinimumLength = 6)]
        public string NewPassWord { get;set; }

        [Compare("NewPassWord",ErrorMessage = "两次密码输入要一致")]
        [DataType(DataType.Password)]
        [Display(Name = "确认新密码")]
        public string ConfirmNewPassWord { get;set; }
    }
}
