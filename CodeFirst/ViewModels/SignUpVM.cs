﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class SignUpVM
    {
        [Required(ErrorMessage = "用户登录名不能为空")]
        [Display(Name = "登录名")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "姓不能为空")]
        [Display(Name = "姓")]
        public string FisrtName { get; set; }

        [Required(ErrorMessage = "名不能为空")]
        [Display(Name = "名")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "电子邮件不能为空")]
        [Display(Name = "电子邮箱")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$",ErrorMessage = "请输入正确的Email格式")]
        public string Email { get; set; }

        [Required(ErrorMessage = "用户登录名不能为空")]
        [Display(Name = "密码")]
        [DataType(DataType.Password)]
        [StringLength(12,ErrorMessage = "{0}长度不能小于{1}大于{2}位",MinimumLength = 6)]
        public string PassWord { get; set; }
        
        [Display(Name = "确认密码")] 
        [DataType(DataType.Password)]
        [Compare("PassWord",ErrorMessage = "两次密码输入不一致")]
        public string ConfirmPassWord { get; set; }
    }
}
