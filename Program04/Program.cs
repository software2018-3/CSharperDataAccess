﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Program04
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //添加一条记录到TBL_User中
            //删除TBL_User一条记录
            //更新

            var connString = ConfigurationManager.AppSettings["connStr"];

            using (SqlConnection conn = new SqlConnection(connString))
            {
                //创建一个命令对象
                using (SqlCommand cmd = new SqlCommand())
                {
                    //初始化连接
                    cmd.Connection = conn;
                    conn.Open();

                    //cmd.CommandText = "insert TBL_User values('201803001','张小三',1,'123','zhangshan@163.com','2018软件技术3班',null)";
                    //cmd.CommandText = "delete tbl_user where userid='201803001'";
                    cmd.CommandText = "update tbl_user  set class='2018软件技术3班'";

                    cmd.ExecuteNonQuery();
                    Console.WriteLine("执行成功！");
                }
            }
        }
    }
}