﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvinceCitySelect
{
    //省份与城市的实体
    public class AreaInfo
    {
        public string AreaName { get; set; }

        public int AreaPID { get; set; }

        public int AreaID { get; set; }

        public override string ToString()
        {
            return AreaName;
        }
    }
}