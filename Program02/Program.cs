﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Program02
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var connectionString = "server=192.168.50.10; database = library; integrated security=true;Pooling=false;";
            //建立快速连接的方式，优化连接速度
            var connectionStringwithPool = "server=192.168.50.10; database = library; integrated security=true; Pooling=true; Min Pool Size=8";

            Stopwatch sw = new Stopwatch();
            sw.Start();
            var i = 0;
            while (i < 2000)
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                }

                i++;
            }
            sw.Stop();
            Console.WriteLine("连接成功！未使用连接池连接所需要时间{0}毫秒。", sw.ElapsedMilliseconds);

            sw.Reset();
            sw.Start();
            i = 0;
            while (i < 2000)
            {
                using (SqlConnection con = new SqlConnection(connectionStringwithPool))
                {
                    con.Open();
                }

                i++;
            }
            sw.Stop();
            Console.WriteLine("连接成功！使用连接池连接所需要时间{0}毫秒。", sw.ElapsedMilliseconds);
        }
    }
}