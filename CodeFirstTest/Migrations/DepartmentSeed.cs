﻿using DataAccesss;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstTest.Migrations
{
    /// <summary>
    /// 初始化数据
    /// </summary>
    public class DepartmentSeed
    {
        public static void SeedMethod(DataContext context)
        {
            var d1 = new Department()
            {
                Name = "电子信息工程学院",
                CreateDateTime = DateTime.Now,
                Descn = "第一大学院"
            };
            var d2 = new Department()
            {
                Name = "机电工程学院",
                CreateDateTime = DateTime.Now,
                Descn = "第二大学院"
            }; 
            var d3 = new Department()
            {
                Name = "汽车工程学院",
                CreateDateTime = DateTime.Now,
                Descn = "第三大学院"
            };

            context.Departments.Add(d1);
            context.Departments.Add(d2);
            context.Departments.Add(d3);
            context.SaveChanges();
        }
    }
}
