﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

namespace Program01
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // 01.连接数据库
            // 连接字符串的定义
            // server表示服务器名称，可以是ip，可以是主机名
            //database表示数据库名称
            // uid登录名，pwd密码
            // 本机服务器，可用系统认证  integrated security=true
            var connectionString = "server=192.168.50.10; database = library; integrated security=true;";

            //用于系统测试
            Stopwatch sw = new Stopwatch();

            //创建连接对象
            SqlConnection con = new SqlConnection(connectionString);

            //真正进行连接

            try
            {
                //开始计时
                sw.Start();
                con.Open();
                sw.Stop();
                Console.WriteLine("连接成功！连接所需要时间{0}毫秒。", sw.ElapsedMilliseconds);
            }
            catch
            {
                Console.WriteLine("连接失败！");
            }

            Thread.Sleep(3000);
            //关闭连接
            con.Close();
            //释放资源
            con.Dispose();
            Console.WriteLine("连接关闭了！");

            //添加一条记录到TBL_User中
            //删除TBL_User一条记录
            //查询TBL_User中所有的记录
            //一种是查询出DataSet
            //一种是查询出List
        }
    }
}