﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CodeFirsrtDemo.Entity;

namespace CodeFirsrtDemo
{
    /// <summary>
    /// 数据上下文  把实体类映射到DB
    /// </summary>
    public class DataContext:DbContext
    {
        //EF会自动把类名变为它的复数形式，作业映射表的名称
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<WorkPlace> WorkPlaces { get; set; }

        public DbSet<MemberGroup> MemberGroups { get; set; }
        public DbSet<Member> Members { get; set; }
    }
}
