﻿namespace CodeFirsrtDemo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CodeFirsrtDemo.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CodeFirsrtDemo.DataContext context)
        {
            ////清理员工  先删除外键实体
            //context.Database.ExecuteSqlCommand("delete employees");
            ////清理岗位
            //context.Database.ExecuteSqlCommand("delete workplaces");
            ////清理部门
            //context.Database.ExecuteSqlCommand("delete departments");

            ////调用种子方法，初始化数据
            //DeptSeed.Seed(context);
            //WpSeed.Seed(context);

            //EmployeeSeed.Seed(context);

            context.Database.ExecuteSqlCommand("delete members");
            context.Database.ExecuteSqlCommand("delete membergroups");
            MemberSeed.Seed(context);
        }
    }
}
