﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirsrtDemo.Entity;

namespace CodeFirsrtDemo.Migrations
{
    public class DeptSeed
    {
        public static void Seed(DataContext context)
        {
            var d1= new Department()
            {
                SortCode = "001",
                Name = "电子信息工程学院",
                Dscn = "电子信息工程学院简介",
                EmployeeCount = 80
            };
            var d2 = new Department()
            {
                SortCode = "002",
                Name = "财经物流学院",
                Dscn = "财经物流学院简介",
                EmployeeCount = 70
            };
            var d3 = new Department()
            {
                SortCode = "003",
                Name = "汽车工程学院",
                Dscn = "汽车工程学院简介",
                EmployeeCount = 60
            };
            var d4 = new Department()
            {
                SortCode = "004",
                Name = "机电工程学院",
                Dscn = "机电工程学院简介",
                EmployeeCount = 65
            };
            var d5 = new Department()
            {
                SortCode = "005",
                Name = "艺术学院",
                Dscn = "艺术学院简介",
                EmployeeCount =50
            };
            var d6 = new Department()
            {
                SortCode = "006",
                Name = "人事处",
                Dscn = "人事处",
                EmployeeCount = 10
            };
            var d7 = new Department()
            {
                SortCode = "007",
                Name = "科研处",
                Dscn = "科研处",
                EmployeeCount =8
            };
            context.Departments.Add(d1);
            context.Departments.Add(d2);
            context.Departments.Add(d3);
            context.Departments.Add(d4);
            context.Departments.Add(d5);
            context.Departments.Add(d6);
            context.Departments.Add(d7);
            context.SaveChanges();
        }
    }
}
