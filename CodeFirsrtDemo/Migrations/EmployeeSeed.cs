﻿using CodeFirsrtDemo.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Migrations
{
    public class EmployeeSeed
    {
        //员工的初始化方法
        public static void Seed(DataContext context)
        {
            var rnd = new Random();
            //每个部门添加10-20名员工
            foreach(var dept in context.Departments.ToList())
            {
                var count = rnd.Next(10,20);
                for(var i=1; i <= count; i++)
                {
                    //添加一名员工
                    var fName = "";
                    var lName = "";
                    _GetRandomChineseName(ref fName, ref lName);
                    var e = new Employee()
                    {
                        SortCode="Lzzy"+dept.SortCode+i.ToString("0000"),
                        FirstName=fName,
                        LastName=lName,
                        FullName=fName+lName,
                        //随机生成性别
                        Sex=Convert.ToBoolean(rnd.Next(0,100)%2),
                        BirthDay=DateTime.Now,
                        //外键如何赋值？ 必须是同一个上下文查询得到的对象 
                        Department=context.Departments.Find(dept.ID)
                    };
                    if (i == 1)
                    {
                        //总监
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "总监"&&n.Department.ID==dept.ID);
                    }
                    else if (i == 2)
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "总经理"&&n.Department.ID==dept.ID);
                    }
                    else if(i==3)
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "经理"&&n.Department.ID==dept.ID);
                    }
                    else if (i ==4)
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "主管" && n.Department.ID == dept.ID);
                    }
                    else if (i == 5||i==6)
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "高级工程师" && n.Department.ID == dept.ID);
                    }
                    else if (i > 6&&i<=10)
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "工程师" && n.Department.ID == dept.ID);
                    }
                    else
                    {
                        e.WorkPlace = context.WorkPlaces.SingleOrDefault(n => n.Name == "助理工程师"&&n.Department.ID==dept.ID);
                    }

                    context.Employees.Add(e);
                    context.SaveChanges();
                    Thread.Sleep(1);
                }
            }
        }

        /// <summary>
        /// 随机生成中文姓名
        /// </summary>
        /// <param name="fName">姓</param>
        /// <param name="lName">名</param>
        private static void _GetRandomChineseName(ref string fName, ref string lName)
        {
            string[] _fNames = new string[]
            {
                "白", "毕", "卞", "蔡", "曹", "岑", "常", "车", "陈", "成", "程", "池", "邓", "丁", "范", "方", "樊", "费", "冯", "符", "傅",
                "甘", "高", "葛", "龚", "古", "关", "郭", "韩", "何", "贺", "洪", "侯", "胡", "华", "黄", "霍", "姬", "简", "江", "姜", "蒋",
                "金", "康", "柯", "孔", "赖", "郎", "乐", "雷", "黎", "李", "连", "廉", "梁", "廖", "林", "凌", "刘", "柳", "龙", "卢", "鲁",
                "陆", "路", "吕", "罗", "骆", "马", "梅", "孟", "莫", "母", "穆", "倪", "宁", "欧", "区", "潘", "彭", "蒲", "皮", "齐", "戚",
                "钱", "强", "秦", "丘", "邱", "饶", "任", "沈", "盛", "施", "石", "时", "史", "司徒", "苏", "孙", "谭", "汤", "唐", "陶",
                "田", "童", "涂", "王", "危", "韦", "卫", "魏", "温", "文", "翁", "巫", "邬", "吴", "伍", "武", "席", "夏", "萧", "谢", "辛",
                "邢", "徐", "许", "薛", "严", "颜", "杨", "叶", "易", "殷", "尤", "于", "余", "俞", "虞", "元", "袁", "岳", "云", "曾", "詹",
                "张", "章", "赵", "郑", "钟", "周", "邹", "朱", "褚", "庄", "卓"
            };

            string _lNames = @"是一种优秀的数据打包和数据交换的形式在当今大行于天下如果没有听说过它的大名那可真是孤陋寡闻了用描述数据的优势显而易见它具有结构简单便于人和机器阅读的双重功效并弥补了关系型数据对客观世界中真实数据描述能力的不足组织根据技术领域的需要制定出了的格式规范并相应的建立了描述模型简称各种流行的";

            var rnd = new Random(DateTime.Now.Millisecond);
            fName = _fNames[rnd.Next(_fNames.Length - 1)];
            lName = _lNames.Substring(rnd.Next(0, _lNames.Length - 1), 1) +
                    _lNames.Substring(rnd.Next(0, _lNames.Length - 1), 1);
        }
    }
}
