﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirsrtDemo.Entity;

namespace CodeFirsrtDemo.Migrations
{
    public class MemberSeed
    {
        public static void Seed(DataContext context)
        {
            //初始化会员
            for (var i = 1; i <= 100; i++)
            {
                var m = new Member()
                {
                    UserName = "user" + i.ToString("000"),
                    Phone = "1383388" + i.ToString("000"),
                    NickName = i.ToString("000"),
                    Sign = "",
                    Avatar = ""
                };
                context.Members.Add(m);
            }
            context.SaveChanges();

            //初始化分组
            var g1 = new MemberGroup()
            {
                Name = "普通会员",
                SortCode = "001"
            };
            var g2 = new MemberGroup()
            {
                Name = "VIP会员",
                SortCode = "009"
            };

            g1.Members = new List<Member>();
            //把会员包含进对应的分组
            foreach (var item in context.Members.Where(n=>!n.UserName.Contains("9")).ToList())
                g1.Members.Add(context.Members.Find(item.ID));
            context.MemberGroups.Add(g1);
            context.SaveChanges();

            //把会员包含到vip组
            g2.Members = new List<Member>();
            foreach (var item in context.Members.Where(n=>n.UserName.Contains("9")).ToList())
                g2.Members.Add(context.Members.Find(item.ID));
            context.MemberGroups.Add(g2);
            context.SaveChanges();
        }
    }
}
