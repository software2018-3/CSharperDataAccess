﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirsrtDemo.Entity;

namespace CodeFirsrtDemo.Migrations
{
    public class WpSeed
    {
        public static void Seed(DataContext context)
        {
            foreach (var department in context.Departments.ToList())
            {
                var wp1 = new WorkPlace()
                {
                    Name = "总监",
                    WPCode = "P10",
                    Salary = 1500000M,
                    Bonus = 14000000M,
                };
                var wp2 = new WorkPlace()
                {
                    Name = "总经理",
                    WPCode = "P9",
                    Salary = 80000M,
                    Bonus = 1000000M,
                };
                var wp3 = new WorkPlace()
                {
                    Name = "经理",
                    WPCode = "P8",
                    Salary = 500000M,
                    Bonus = 400000M,
                };
                var wp4 = new WorkPlace()
                {
                    Name = "主管",
                    WPCode = "P7",
                    Salary = 300000M,
                    Bonus = 2000000M,
                };
                var wp5 = new WorkPlace()
                {
                    Name = "高级工程师",
                    WPCode = "P7",
                    Salary = 200000M,
                    Bonus = 1000000M,
                };
                var wp6 = new WorkPlace()
                {
                    Name = "工程师",
                    WPCode = "P6",
                    Salary = 100000M,
                    Bonus = 20000M,
                };
                var wp7 = new WorkPlace()
                {
                    Name = "助理工程师",
                    WPCode = "P5",
                    Salary = 60000M,
                    Bonus = 5000M,
                };
                //把岗位关联对应的部门
                wp1.Department = context.Departments.Find(department.ID);
                wp2.Department = context.Departments.Find(department.ID);
                wp3.Department = context.Departments.Find(department.ID);
                wp4.Department = context.Departments.Find(department.ID);
                wp5.Department = context.Departments.Find(department.ID);
                wp6.Department = context.Departments.Find(department.ID);
                wp7.Department = context.Departments.Find(department.ID);

                context.WorkPlaces.Add(wp1);
                context.WorkPlaces.Add(wp2);
                context.WorkPlaces.Add(wp3);
                context.WorkPlaces.Add(wp4);
                context.WorkPlaces.Add(wp5);
                context.WorkPlaces.Add(wp6);
                context.WorkPlaces.Add(wp7);
                context.SaveChanges();
            }
        }
    }
}
