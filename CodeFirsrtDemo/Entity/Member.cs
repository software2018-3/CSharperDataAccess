﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Entity
{
    /// <summary>
    /// 会员 
    /// </summary>
    public class Member
    {
        public Guid ID { get; set; }

        public string UserName { get; set; }
        public string Phone { get; set; }
        public string NickName { get; set; }
        //个性签名
        public string Sign { get; set; }
        //头像
        public string Avatar { get; set; }

        public Member()
        {
            ID = Guid.NewGuid();
        }
    }
}
