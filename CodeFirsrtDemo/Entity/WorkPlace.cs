﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Entity
{
    /// <summary>
    /// 职位 岗位
    /// </summary>
    public class WorkPlace
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        //工资
        public decimal Salary { get; set; }
        //奖金
        public decimal Bonus { get; set; }
        // 岗位编号
        public string WPCode { get; set; }

        //创建时间 
        public DateTime CreateDateTime { get; set; } = DateTime.Now;

        //关联到部门
        public virtual  Department Department { get; set; }


        public WorkPlace()
        {
            ID= Guid.NewGuid();
        }
    }
}
