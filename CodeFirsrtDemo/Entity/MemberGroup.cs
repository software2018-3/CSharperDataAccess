﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Entity
{
    /// <summary>
    /// 会员组
    /// </summary>
    public class MemberGroup
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string SortCode { get; set; }
        //建立包含关系  ICollection 不能用IList
        public virtual  ICollection<Member> Members { get; set; }

        public MemberGroup()
        {
            ID = Guid.NewGuid();
        }
    }
}
