﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Entity
{
    public class Employee
    {
        public Guid ID { get; set; }
        public string SortCode { get; set; }
        public string FirstName { get; set; }
        public  string LastName { get; set; }
        public string FullName { get; set;}

        public bool Sex { get; set; } = true;
        public DateTime BirthDay { get; set; }=DateTime.MinValue;
        public string Address { get; set; }
        public string Phone { get; set; }

        //归属到部门  必须加上virtuel
        public virtual Department Department { get; set; }

        //岗位
        public virtual  WorkPlace WorkPlace { get; set; }

        public Employee()
        {
            ID = Guid.NewGuid();
        }
    }
}
