﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirsrtDemo.Entity
{
    /// <summary>
    /// 二级学院的实体   通过EF将映射为数据表
    /// </summary>
    public class Department
    {
        //实体的主键  属性主键的命名规则  ID  Id  类名+ID  类名+Id
        public Guid ID { get; set; }

        public string SortCode { get; set; }

        //二级学院的名称
        public string Name { get; set; }

        //说明
        public string Dscn { get; set; }

        //员工人数
        public int EmployeeCount { get; set; } = 1;

        public Department()
        {
            ID = Guid.NewGuid();
        }
    }
}
