﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirsrtDemo.Entity;

namespace CodeFirsrtDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new DataContext();
            foreach (var g in context.MemberGroups.ToList())
            {
                Console.WriteLine("分组编号：{0}   名称：{1}   会员数量：{2}",g.SortCode, g.Name,g.Members.Count);
                Console.WriteLine("================会员列表=================");
                foreach (var member in g.Members)
                {
                    Console.WriteLine("昵称：{0}  手机{1}", member.NickName,member.Phone);
                }
                Console.WriteLine("=====================================");
            }


            //foreach(var dept in context.Departments.OrderBy(n=>n.SortCode).ToList())
            //{
            //    var count = context.Employees.Where(n => n.Department.ID == dept.ID).Count();
            //    Console.WriteLine("{0}{1},员工共{2}人", dept.SortCode, dept.Name, count);
            //    Console.WriteLine("=====================================");
            //    //打印该部门所有员工
            //    //获取该部门所有的员工
            //    var persons = context.Employees.Where(x => x.Department.ID == dept.ID).OrderBy(x => x.SortCode).ToList();
            //    foreach (var p in persons)
            //        Console.WriteLine("岗位：{0}  员工编号：{1}   姓名：{2}    性别：{3}    生日：{4}", p.WorkPlace.Name, p.SortCode,
            //            p.FullName, p.Sex ? "男" : "女", p.BirthDay.ToString("yyyyMMdd"));
            //    Console.WriteLine("=====================================");

            //    ////显示各部门的岗位
            //    //var wp = context.WorkPlaces.Where(n => n.Department.ID == dept.ID).OrderBy(n=>n.WPCode).ToList();
            //    //foreach (var item in wp)
            //    //{
            //    //    Console.WriteLine("部门名称{0}，岗位编号{1}，岗位名称{2}", item.Department.Name,item.WPCode, item.Name);
            //    //}

            //    Console.ReadKey(true);
            //}

            ////查询 显示所有的二级学院
            //Console.WriteLine("原始数据");
            //var deptList = context.Departments.ToList();
            //var i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");

            //Console.WriteLine("按编号顺序排序");

            //deptList = context.Departments.OrderBy(x => x.SortCode).ToList();
            //i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");

            //Console.WriteLine("按编号逆序排序");

            //deptList = context.Departments.OrderByDescending(x => x.SortCode).ToList();
            //i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");

            //Console.WriteLine("先按编号正序排序，再按名称逆序排序");
            //deptList = context.Departments.OrderBy(x => x.SortCode)
            //    .ThenByDescending(n=>n.Name)
            //    .ThenBy(n=>n.Dscn).ToList();

            //i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");
            
            //Console.WriteLine("按条件查询:查询所有学院的记录");
            //deptList = context.Departments.Where(x => x.Name.Contains("学院")).OrderBy(x => x.SortCode).ToList();
            //i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");

            //Console.WriteLine("按条件查询:查询科研处的记录");
            //deptList = context.Departments.Where(n => n.Name != "科研处").ToList();
            //i = 1;
            //foreach (var item in deptList)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name + " " + item.Dscn+" "+item.EmployeeCount+"人");

            //Console.WriteLine("查询出员工最多的学院");
            ////查询最多员工的值
            //int max = context.Departments.Max(n => n.EmployeeCount);
            //Console.WriteLine(max.ToString());
            //var avg =  context.Departments.Average(n => n.EmployeeCount);
            //Console.WriteLine(avg.ToString());
            //var sum = context.Departments.Sum(n => n.EmployeeCount);
            //Console.WriteLine(sum.ToString());

            ////查询出最多员工的记录
            ////deptList = context.Departments.Where(n => n.EmployeeCount == max).ToList();
            //var dept = context.Departments.OrderByDescending(x => x.EmployeeCount).FirstOrDefault();
            //Console.WriteLine(i++ + " " + dept.SortCode + " " + dept.Name + " " + dept.Dscn+" "+dept.EmployeeCount+"人");
            
            //Console.WriteLine("查询结果选择字段");
            ////使用select动态添加新的对象集合

            //var list = context.Departments.Select(s => new { s.SortCode, s.Name }).ToList();
            //var  i = 1;
            //foreach (var item in list)
            //    Console.WriteLine(i++ + " " + item.SortCode + " " + item.Name);


            //Console.WriteLine("添加记录到学院表中...");
            ////添加记录前执行sql命令去清除所的记录
            //context.Database.ExecuteSqlCommand("delete departments");

            ////添加一条记录
            //var department1 = new Department()
            //{
            //    Name = "电子信息工程学院",
            //    Dscn = "软件技术专业……"
            //};
            //var department2 = new Department()
            //{
            //    Name = "艺术学院",
            //    Dscn = "广告设计专业……"
            //};
            ////添加此对象到上下文，保存更改
            //context.Departments.Add(department1);
            //context.Departments.Add(department2);
            //context.SaveChanges();
            //Console.WriteLine("添加成功！");

            //修改一条记录
            //在数据上下文中找到要修改的对象 
            // lamda表达式中查询单个对象的方法 SingleOrDefault,  Find
            // var department = context.Departments.SingleOrDefault(x => x.Name == "艺术学院");
            //var department = context.Departments.Find(Guid.Parse("09EA0038-EE37-41BB-9FD3-5D8084CD38DF"));
            //department.Dscn = "原艺术设计系，有广告设计专业、服装专业...";
            //context.SaveChanges();

            ////删除一条记录
            //var del =context.Departments.SingleOrDefault(x => x.Name == "艺术学院");
            //context.Departments.Remove(del);
            //context.SaveChanges();

            Console.ReadKey();
        }
    }
}
