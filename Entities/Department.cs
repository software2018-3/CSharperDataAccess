﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    /// <summary>
    /// 部门实体类
    /// </summary>
    public class Department
    {
        //当属性名称为id  Id  ID或是类名+ID时，缺省将其看成是所映射的数据库表的PK
        public Guid DepartmentID { get; set; }

        public string Name { get; set; }
        public string Descn { get; set; }

        public DateTime CreateDateTime { get; set; }

        public Department()
        {
            DepartmentID = Guid.NewGuid();
        }

    }
}
