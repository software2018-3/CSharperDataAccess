﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginLockDemo
{
    public class UserInfo
    {
        public Guid UserID { get; set; }
        public string UserName { get; set; }

        public string PassWord { get; set; }
        public bool IsLocked { get; set; } = false;
        public int ErrorTimes { get; set; } = 0;

        public DateTime? LastErrorDateTime { get; set; } = null;

        public UserInfo()
        {
            UserID = Guid.NewGuid();
        }
    }
}