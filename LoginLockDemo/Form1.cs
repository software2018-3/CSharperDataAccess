﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginLockDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //添加测试用户
            //Test();

            //1.用用户名和密码查询是否存在对应的Userinfo的记录，判断是否被锁定，存在即登陆成功

            //先查询是否存在此用户
            var connStr = ConfigurationManager.AppSettings["connStr"];
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = @"select * from userinfo where username='" + txtUserName.Text + "'";

                    UserInfo user = null;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        //把查询出来数据赋值到user对象
                        if (rdr.Read())
                        {
                            user = new UserInfo();
                            user.UserID = rdr.GetGuid(0);
                            user.UserName = rdr.GetString(1);
                            user.PassWord = rdr.GetString(2);
                            user.IsLocked = rdr.GetBoolean(3);
                            user.ErrorTimes = rdr.GetInt32(4);
                            user.LastErrorDateTime = rdr.IsDBNull(5) ? DateTime.Parse("1900-1-1") : rdr.GetDateTime(5);
                        }
                    }  //SqlDataReader资源释放

                    if (user == null)
                    {
                        //不存在此用户
                        MessageBox.Show("不存在此注册用户，请检查您的用户名！");
                        return;
                    }

                    user = null;
                    //用户名和密码一起验证
                    cmd.CommandText = @"select * from userinfo where username='" + txtUserName.Text +
                                      "' and [Password]='" + Encryption.GetEncryptString(txtPwd.Text) + "'";
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            user = new UserInfo();
                            user.UserID = rdr.GetGuid(0);
                            user.UserName = rdr.GetString(1);
                            user.PassWord = rdr.GetString(2);
                            user.IsLocked = rdr.GetBoolean(3);
                            user.ErrorTimes = rdr.GetInt32(4);
                            user.LastErrorDateTime = rdr.IsDBNull(5) ? DateTime.Parse("1900-1-1") : rdr.GetDateTime(5);
                        }
                    }
                    //如果查到有记录，校验是否锁定
                    if (user != null && user.IsLocked)
                    {
                        MessageBox.Show("改用户被锁定，请与管理员联系！");
                        return;
                    }

                    if (user != null && !user.IsLocked)
                    {
                        //把错误次数清空
                        cmd.CommandText = "update UserInfo set LastErrorDateTime=NULL, ErrorTimes=0 where UserId=" + user.UserID;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("登录成功！");
                        return;
                    }

                    //如果查不到用户，说明用户名和密码错误，更新错误次数，并且记录错误时间
                    if (user == null)
                    {
                        //修改错误次数和错误时间
                        cmd.CommandText =
                            @"update userinfo set ErrorTimes=ErrorTimes+1, LastErrorDateTime=getdate() where username='" +
                            txtUserName.Text.Trim() + "'";
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("用户名和密码错误！");

                        //校验错误次数和错误时间，是否需要锁定此用户
                        cmd.CommandText = @"select * from userinfo where username='" + txtUserName.Text + "'";

                        user = null;
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            //把查询出来数据赋值到user对象
                            if (rdr.Read())
                            {
                                user = new UserInfo();
                                user.UserID = rdr.GetGuid(0);
                                user.UserName = rdr.GetString(1);
                                user.PassWord = rdr.GetString(2);
                                user.IsLocked = rdr.GetBoolean(3);
                                user.ErrorTimes = rdr.GetInt32(4);
                                user.LastErrorDateTime = rdr.IsDBNull(5) ? DateTime.Parse("1900-1-1") : rdr.GetDateTime(5);
                            }
                        }  //SqlDataReader资源释放
                        //查询改用户账户错误次数是否>4  上次错误是否在5分钟以内,把IsLocked更新为1
                        if (user.ErrorTimes >= 5 && DateTime.Now.Subtract(Convert.ToDateTime(user.LastErrorDateTime)).Minutes < 5)
                        {
                            cmd.CommandText = @"update userinfo set islocked=1 where username='" +
                                              txtUserName.Text.Trim() + "'";
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("该用户名账号被锁定！");
                        }

                        return;
                    }
                }
            }
        }

        private void Test()
        {
            //添加测试数据
            var user = new UserInfo()
            {
                UserName = "xiaomi",
                PassWord = Encryption.GetEncryptString("123456 ")
            };
            var connStr = ConfigurationManager.AppSettings["connStr"];
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    var sqlCmdStr = string.Format("insert userinfo values('{0}','{1}','{2}',{3},{4},NULL)",
                        user.UserID, user.UserName, user.PassWord, Convert.ToInt32(user.IsLocked), user.ErrorTimes);
                    cmd.CommandText = sqlCmdStr;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("添加成功！");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtUserName.Text = txtPwd.Text = string.Empty;
            txtUserName.Focus();
        }
    }
}