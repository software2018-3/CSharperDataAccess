﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Program03
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var version = ConfigurationManager.AppSettings["Version"];
            var author = ConfigurationManager.AppSettings["Author"];
            Console.WriteLine(version);
            Console.WriteLine(author);

            //从配置文件中读取连接串
            var connStr = ConfigurationManager.AppSettings["sqlConStr"];
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var i = 0;
            while (i < 2000)
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    con.Open();
                }

                i++;
            }
            sw.Stop();
            Console.WriteLine("连接成功！未使用连接池连接所需要时间{0}毫秒。", sw.ElapsedMilliseconds);

            var connStr1 = ConfigurationManager.ConnectionStrings["sqlConStr1"].ConnectionString;
            sw.Reset();
            sw.Start();
            i = 0;
            while (i < 2000)
            {
                using (SqlConnection con = new SqlConnection(connStr1))
                {
                    con.Open();
                }

                i++;
            }
            sw.Stop();
            Console.WriteLine("连接成功！未使用连接池连接所需要时间{0}毫秒。", sw.ElapsedMilliseconds);
        }
    }
}